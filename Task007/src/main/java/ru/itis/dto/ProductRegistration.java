package ru.itis.dto;

import lombok.AllArgsConstructor    ;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ProductRegistration {

    private String name;
    private String colour;
    private Integer amount;
}
