package ru.itis.services;

import ru.itis.dto.ProductRegistration;
import ru.itis.models.Product;
import ru.itis.repositories.ProductRepository;

class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository studentsRepository) {
        this.productRepository = studentsRepository;
    }

    @Override
    public void registration(ProductRegistration form) {
        Product product = Product.builder()
                .name(form.getName())
                .colour(form.getColour())
                .amount(form.getAmount())
                .build();

        productRepository.save(product);

    }
}

