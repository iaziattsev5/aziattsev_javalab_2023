package ru.itis.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.itis.models.Product;

import javax.sql.DataSource;
import java.util.*;

public class ProductRepositoryImpl implements ProductRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_Products = "select * from Product;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from Product where id = :id";

    private static final RowMapper<Product> ProductMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .name(row.getString("name"))
            .colour(row.getString("colour"))
            .amount(row.getObject("amount", Integer.class))
            .build();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProductRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_Products, ProductMapper);
    }

    @Override
    public void save(Product Product) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("name", Product.getName());
        paramsAsMap.put("colour", Product.getColour());
        paramsAsMap.put("amount", Product.getAmount());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("Product")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();


        Product.setId(id);
    }

    @Override
    public Optional<Product> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    ProductMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(Product Product) {

    }

    @Override
    public void delete(Long id) {

    }
}
