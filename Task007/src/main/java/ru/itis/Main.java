package ru.itis;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.itis.models.Product;
import ru.itis.repositories.ProductRepository;
import ru.itis.repositories.ProductRepositoryImpl;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.username"));
        config.setPassword(properties.getProperty("db.password"));
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

        ProductRepository ProductRepository = new ProductRepositoryImpl(dataSource);
        System.out.println(ProductRepository.findAll());

        ExecutorService executorService = Executors.newFixedThreadPool(50);

        //for (int i = 0; i < 10_000; i++) {
        //    executorService.submit(() -> {
        //        try {
        //            ProductRepository.save(Product.builder()
        //                    .name("Микроволновка")
        //                    .colour("Зеленый")
        //                    .build());
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //            System.exit(0);
        //        }
        //    });
        //}

    }
}

