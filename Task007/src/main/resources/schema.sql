drop table if exists product;

create table product
(
    id bigserial primary key,
    name varchar(20),
    colour varchar(20),
    amount integer
);