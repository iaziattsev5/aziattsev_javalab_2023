package ru.itis.constants;

public class Paths {
    public static final String APPLICATION_PREFIX = "/app";

    public static final String SIGN_UP_PATH = "/signUp";
    public static final String USERS_PATH = "/users";

    public static final String SEARCH_PAGE = "/users/search.html";

    public static final String USERS_SEARCH_PATH = "/users/search";
}
