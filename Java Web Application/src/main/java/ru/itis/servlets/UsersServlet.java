package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.SignUpDto;
import ru.itis.dto.converters.HttpFormsConverter;
import ru.itis.services.UsersService;

import java.io.IOException;

import static ru.itis.constants.Paths.*;

@WebServlet(name = "usersServlet", urlPatterns = {USERS_PATH, SIGN_UP_PATH}, loadOnStartup = 1)
public class UsersServlet extends HttpServlet {
    private UsersService usersService;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.usersService = context.getBean(UsersService.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_PATH)) {
            SignUpDto signUpData = HttpFormsConverter.from(request);

            usersService.signUp(signUpData);

            response.sendRedirect(APPLICATION_PREFIX + SEARCH_PAGE);
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
