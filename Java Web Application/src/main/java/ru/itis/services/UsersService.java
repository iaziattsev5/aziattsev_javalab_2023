package ru.itis.services;

import ru.itis.dto.SignUpDto;
import ru.itis.models.User;

import java.util.List;

public interface UsersService {
    void signUp(SignUpDto signUpData);
    List<User> getAllUsers();

    List<User> getAllUsersByAge(int ageFrom, int ageTo);
}


