package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.SignUpDto;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;
import ru.itis.services.UsersService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    @Override
    public void signUp(SignUpDto signUpData) {
        User user = User.builder()
                .firstName(signUpData.getFirstName())
                .lastName(signUpData.getLastName())
                .email(signUpData.getEmail())
                .password(signUpData.getPassword())
                .build();

        usersRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public List<User> getAllUsersByAge(int ageFrom, int ageTo) {
        return usersRepository.findAllByAgeInRangeOrderByIdDesc(ageFrom, ageTo);
    }
}
