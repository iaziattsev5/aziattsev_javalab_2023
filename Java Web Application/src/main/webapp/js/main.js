function searchUsers(query) {
    return fetch('/app/users/search?query=' + query)
        .then((response) => {
            return response.json()
        }).then((users) => {
            fillTable(users)
        })
}

function fillTable(users) {
    let table = document.getElementById("usersTable");

    table.innerHTML = '    <tr>\n' +
        '        <th>id</th>\n' +
        '        <th>First Name</th>\n' +
        '        <th>Last Name</th>\n' +
        '    </tr>';

    for (let i = 0; i < users.length; i++) {
        let row = table.insertRow(-1);
        let idCell = row.insertCell(0);
        let firstNameCell = row.insertCell(1);
        let lastNameCell = row.insertCell(2);

        idCell.innerHTML = users[i].id;
        firstNameCell.innerHTML = users[i].firstName;
        lastNameCell.innerHTML = users[i].lastName;
    }
}
