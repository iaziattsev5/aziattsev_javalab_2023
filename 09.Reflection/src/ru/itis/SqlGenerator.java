package ru.itis;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SqlGenerator {

    public static <T> String getTableName(Class<T> entityClass) {
        String tableName = "";
        if (entityClass.isAnnotationPresent(TableName.class)) {
            TableName tableNameAnnotation = entityClass.getAnnotation(TableName.class);
            tableName = tableNameAnnotation.value();
        } else {
            throw new IllegalArgumentException("There is no @TableName annotation!");
        }
        return tableName;
    }
    public <T> String createTable(Class<T> entityClass) {
        String tableName = getTableName(entityClass);

        List<Column> columnList = new ArrayList();
        Field[] fields = entityClass.getDeclaredFields();

        for (Field field : fields) {
            int modifiers = field.getModifiers();
            if (Modifier.isPrivate(modifiers)) {
                try {
                    Column column = Column.fromField(field);
                    if (column != null) {
                        columnList.add(column);
                    }
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        String header = "CREATE TABLE " + "IF NOT EXISTS " +
                "`" + tableName + "`" + " (\n";


        int primaryKeyCount = 0;
        String primaryKeyName = "";

        List<String> columnStringList = new ArrayList();

        for (Column column : columnList) {
            if ((column.isPrimaryKey) || (column.identity)) {
                primaryKeyCount++;
                primaryKeyName = column.name;
            }
            columnStringList.add(column.toString());
        }

        String footer = "";
        if (primaryKeyCount == 1) {
            footer = ",\nPRIMARY KEY (" + "`" + primaryKeyName + "`" + "))";
        } else {
            footer = ")";
        }

        return header + String.join(",\n", columnStringList) + footer + ";";
    }

    public String insert(Object entity) {

        String tableName = getTableName(entity.getClass());

        List<String> setValueStringList = new ArrayList();
        Field[] fields = entity.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            Column column = Column.fromField(field);
            if ((column != null) && (!(column.identity))) {
                Object value;
                try {
                    value = field.get(entity);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }

                if ((value != null)) {
                    String setValueString = "`" + column.name + "`"
                            + "=" + value;

                    setValueStringList.add(setValueString);

                }
            }
        }

        String header = "INSERT " + "INTO " + "`" + tableName + "`" + " SET ";
        String result = header + String.join(", ", setValueStringList) + ";";

        return result;
    }
}
