package ru.itis;

public class Main {
    public static void main(String[] args) {
        SqlGenerator sqlGenerator = new SqlGenerator();

        String sqlTable = sqlGenerator.createTable(User.class);

        User user = new User(2L, "name", "secondname", true);

        System.out.println(sqlTable);
        System.out.println();
        System.out.println(sqlGenerator.insert(user));

    }
}

