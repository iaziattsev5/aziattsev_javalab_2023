package ru.itis;

@TableName(value = "table")
public class User {
    @ColumnName(value = "id", primary = true, identity = true)
    private Long id;
    @ColumnName(value = "first_name", maxLength = 25)
    private String firstName;
    @ColumnName(value = "last_name")
    private String lastName;
    @ColumnName(value = "is_worker", defaultBoolean = true)
    private boolean isWorker;

    public User() {};

    public User(Long id, String firstName, String lastName, boolean isWorker) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isWorker = isWorker;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isWorker=" + isWorker +
                '}';
    }
}
