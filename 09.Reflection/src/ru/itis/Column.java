package ru.itis;

import java.lang.constant.Constable;
import java.lang.reflect.Field;

public class Column {
    public String name;
    public ColumnType columnType;
    public boolean isPrimaryKey;
    public boolean defaultBoolean;
    public boolean identity;
    public int maxLength;

    public enum ColumnType {
        BOOL,
        INT,
        BIGINT,
        VARCHAR,

        OTHER;

        public String toString() {
            switch (this) {
                case BOOL:
                    return " BOOL";
                case INT:
                    return " INT";
                case VARCHAR:
                    return " VARCHAR";
                case BIGINT:
                    return " BIGINT";
            }
            return "OTHER";
        }
    }


    public static Column fromField(Field field) {
        Class fieldType = field.getType();
        ColumnType columnType;
        if ((fieldType == Boolean.TYPE) || (fieldType == Boolean.class)) {
            columnType = ColumnType.BOOL;
        } else if ((fieldType == Integer.TYPE) || (fieldType == Integer.class)) {
            columnType = ColumnType.INT;
        } else if ((fieldType == Long.TYPE) || (fieldType == Long.class)) {
            columnType = ColumnType.BIGINT;
        } else if (fieldType == String.class) {
            columnType = ColumnType.VARCHAR;
        } else {
            return null;
        }

        Column column = new Column();
        column.columnType = columnType;


        if (field.isAnnotationPresent(ColumnName.class)) {
            ColumnName columnName = field.getAnnotation(ColumnName.class);
            String name = columnName.value();
            if (!name.isEmpty()) {
                column.name = name;
            }
            column.isPrimaryKey = columnName.primary();
            column.defaultBoolean = columnName.defaultBoolean();
            column.identity = columnName.identity();
            column.maxLength = columnName.maxLength();
        } else {
            throw new IllegalArgumentException("There is no @ColumnName annotation!");
        }

        return column;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("`");
        result.append(name);
        result.append("`");
        result.append(columnType.toString());
        return result.toString();
    }
}
